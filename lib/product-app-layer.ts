import * as cdk from 'aws-cdk-lib';
import { Construct } from "constructs";
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as ssm from 'aws-cdk-lib/aws-ssm'; // ssm: serve para criar parâmetros no AWS Systems Manager Parameter Store

export class ProductAppLayerStack extends cdk.Stack {
    readonly productsLayers: lambda.LayerVersion
    constructor(scope: Construct, id: string, props?: cdk.StackProps){
        super(scope, id, props)

        this.productsLayers = new lambda.LayerVersion(this, 'ProductsLayer', {
            code: lambda.Code.fromAsset('lambda/products/layers/productsLayer'),
            compatibleRuntimes: [lambda.Runtime.NODEJS_16_X],
            description: 'Products layers',
            layerVersionName: 'ProductsLayer',
            removalPolicy: cdk.RemovalPolicy.RETAIN,
            license: 'MIT'
        })

        new ssm.StringParameter(this, 'ProductsLayerVersionArn', {
            parameterName: '/ecommerce/products-layers-version-arn',
            description: 'Products layers version ARN',
            stringValue: this.productsLayers.layerVersionArn
        })
    }

}

