import * as lambdaNodeJs from 'aws-cdk-lib/aws-lambda-nodejs'
import * as cdk from 'aws-cdk-lib'
import * as apigateway from 'aws-cdk-lib/aws-apigateway'
import * as cwlogs from 'aws-cdk-lib/aws-logs'
import { Construct } from 'constructs'

interface EcommerceApiStackProps extends cdk.StackProps {
    productsFetchHandler: lambdaNodeJs.NodejsFunction
    productsAdminHandler: lambdaNodeJs.NodejsFunction
}
export class ApiGatewayStack extends cdk.Stack {
    constructor(scope: Construct, id: string, props: EcommerceApiStackProps){
        super(scope, id, props)

        const logGroup = new cwlogs.LogGroup(this, 'EcommerceAPIAccessLogGroup', {})

        const api = new apigateway.RestApi(this, 'EcommerceAPI', {
            restApiName: 'Ecommerce API',
            description: 'Ecommerce REST API',
            cloudWatchRole: true,
            deployOptions: {
                accessLogDestination: new apigateway.LogGroupLogDestination(logGroup),
                accessLogFormat: apigateway.AccessLogFormat.jsonWithStandardFields({
                    caller: true,
                    httpMethod: true,
                    ip: false,
                    protocol: true,
                    requestTime: true,
                    status: true,
                    user: false,
                    resourcePath: true,
                    responseLength: true
                })
            }
        })

        const productsFetchIntegration =
            new apigateway.LambdaIntegration(props.productsFetchHandler, {})

        const productsAdminIntegration =
            new apigateway.LambdaIntegration(props.productsAdminHandler, {})

        // api.root é a rota raiz da API
        // "/products"
        const productsResource =
            api.root.addResource('products')

        // "/products/{id}"
        const productIdResource = productsResource.addResource('{id}')

        // GET /products/{id}
        productsResource.addMethod('GET', productsFetchIntegration, {})

        // GET /products/{id}
        productIdResource.addMethod('GET', productsFetchIntegration, {})

        // POST /products
        productsResource.addMethod('POST', productsAdminIntegration, {})

        // PUT /products/{id}
        productIdResource.addMethod('PUT', productsAdminIntegration, {})

        // DELETE /products/{id}
        productIdResource.addMethod('DELETE', productsAdminIntegration, {})
    }
}

