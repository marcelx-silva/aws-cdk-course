import * as lambda from 'aws-cdk-lib/aws-lambda'
import * as lambdaNodeJs from 'aws-cdk-lib/aws-lambda-nodejs'
import * as cdk from 'aws-cdk-lib'
import { Construct } from 'constructs'
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb'
import * as ssm from 'aws-cdk-lib/aws-ssm'

export class ProductsAppStack extends cdk.Stack {

    readonly productsFetchHandler: lambdaNodeJs.NodejsFunction
    readonly productsAdminHandler: lambdaNodeJs.NodejsFunction
    readonly productsTable: dynamodb.Table
    constructor(scope: Construct, id: string, props?:cdk.StackProps){
        super(scope, id, props)

        this.productsTable = new dynamodb.Table(this, 'ProductsTable', {
            tableName: 'products',
            removalPolicy: cdk.RemovalPolicy.DESTROY,
            partitionKey: {
                name: 'id',
                type: dynamodb.AttributeType.STRING
            },
            billingMode: dynamodb.BillingMode.PROVISIONED,
            readCapacity: 1,
            writeCapacity: 1
        })

        // Product Layer

        const productsLayerArn = ssm.StringParameter.valueForStringParameter(this, '/ecommerce/products-layers-version-arn')
        const productsLayer = lambda.LayerVersion.fromLayerVersionArn(this, 'ProductsLayer', productsLayerArn)

        this.productsFetchHandler = new lambdaNodeJs.NodejsFunction(this, 'ProductsFetchHandler', {
            runtime: lambda.Runtime.NODEJS_16_X,
            functionName: 'products-fetch-handler',
            description: 'Lambda function to fetch products from DynamoDB',
            entry: 'lambda/products/fetch.ts',
            handler: 'handler',
            memorySize: 128,
            timeout: cdk.Duration.seconds(5),
            bundling: { // bundling: serve para configurar o webpack. O webpack é um empacotador de módulos JavaScript
                minify: true,
                sourceMap: false
            },
            environment: {
                PRODUCTS_TABLE_NAME: this.productsTable.tableName
            },
            layers: [productsLayer]
        })

        this.productsTable.grantReadData(this.productsFetchHandler)

        this.productsAdminHandler = new lambdaNodeJs.NodejsFunction(this, 'ProductsAdminHandler', {
            runtime: lambda.Runtime.NODEJS_16_X,
            functionName: 'products-admin-handler',
            description: 'Admin Lambda function to write products from DynamoDB',
            entry: 'lambda/products/admin.ts',
            handler: 'handler',
            memorySize: 128,
            timeout: cdk.Duration.seconds(5),
            bundling: {
                minify: true,
                sourceMap: false
            },
            environment: {
                PRODUCTS_TABLE_NAME: this.productsTable.tableName
            },
            layers: [productsLayer]
        })

        this.productsTable.grantWriteData(this.productsAdminHandler)
    }
}
