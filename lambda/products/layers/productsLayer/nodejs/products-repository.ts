import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { AWSError } from 'aws-sdk/lib/error';
import { v4 as uuid } from 'uuid';

export interface ProductEntity {
    id: string,
    productName: string,
    code: string,
    price: number,
    model: string,
    productURL: string
}

export interface ProductRepository {
    findAll(): Promise<ProductEntity[]>
    findById(id: string): Promise<ProductEntity>
    create(product: ProductEntity): Promise<ProductEntity>
    delete(id: string): Promise<void>
    update(id: string, product: ProductEntity): Promise<ProductEntity>
}

export class ProductRepositoryImpl implements ProductRepository {
    private readonly dynamoDB: DocumentClient
    private readonly tableName: string

    constructor(dynamoDB: DocumentClient, tableName: string) {
        this.dynamoDB = dynamoDB
        this.tableName = tableName
    }
    
    async update(id: string, product: ProductEntity): Promise<ProductEntity> {
        try{
            const params = {
                TableName: this.tableName,
                Key: { id: id },
                ConditionExpression: 'attribute_exists(id)',
                // UPDATED_NEW: retorna apenas os atributos que foram atualizados
                // ALL_NEW: retorna todos os atributos do item após a atualização
                // NONE: não retorna nenhum atributo (padrão)
                // ALL_OLD: retorna todos os atributos do item antes da atualização
                ReturnValues: 'UPDATED_NEW',
                UpdateExpression: 'set productName = :pn, code = :cd, price = :pr, model = :md, productURL = :pUrl',
                ExpressionAttributeValues: {
                    ':pn': product.productName,
                    ':cd': product.code,
                    ':pr': product.price,
                    ':md': product.model,
                    ':pUrl': product.productURL
                }
            }

            const updatedProduct = await this.dynamoDB.update(params).promise()

            return updatedProduct.Attributes as ProductEntity
        }catch(error){
            if ((<AWSError> error).message === 'ConditionalCheckFailedException')
                throw new Error('Product not found')
            else
                throw new Error('Internal server error')
        }
    }

    async delete(id: string): Promise<void> {
        const params = {
            TableName: this.tableName,
            Key: { id: id },
            ReturnValues: 'ALL_OLD'
        }

        const productDeleted = await this.dynamoDB.delete(params).promise()

        if (!productDeleted.Attributes) 
            throw new Error('Product not found')
        
    }
    
    async create(product: ProductEntity): Promise<ProductEntity> {
        const params = {
            TableName: this.tableName,
            Item: { ...product, id: uuid()},
        }

        const createdProduct = await this.dynamoDB.put(params).promise()

        return product
    }

    async findById(id: string): Promise<ProductEntity> {
        const params = {
            TableName: this.tableName,
            Key: { id: id }
        }

        const product = await this.dynamoDB.get(params).promise()

        if (product.Item) {
            return product.Item as ProductEntity
        }else {
            throw new Error('Product not found')
        }
    }
    
    async findAll(): Promise<ProductEntity[]> {
        const params = {
            TableName: this.tableName
        }

        const productsList = await this.dynamoDB.scan(params).promise()

        return productsList.Items as ProductEntity[]
    }
}