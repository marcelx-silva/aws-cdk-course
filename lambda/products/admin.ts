import {APIGatewayProxyEvent, Context} from "aws-lambda";
import { DynamoDB } from "aws-sdk";
import { ProductRepositoryImpl } from "opt/nodejs/productsLayer";

const productsDatabaseName = process.env.PRODUCTS_TABLE_NAME!
const productRepository = new ProductRepositoryImpl(new DynamoDB.DocumentClient(), productsDatabaseName)

export async function handler(event: APIGatewayProxyEvent, context: Context) {
    console.log('Event: ', event)
    console.log('Context: ', context)

    console.log(`Lambda Request ID: ${event.requestContext.requestId}\nEvent Request ID: ${context.awsRequestId}`)

    const eventResource = event.resource
    if (eventResource === '/products'){
        console.log('POST products')

        try {
            const product = JSON.parse(event.body!)
            const productCreated = await productRepository.create(product)

            return {
                statusCode: 200,
                body: JSON.stringify(productCreated)
            }

        }catch(error) {
            return {
                statusCode: 400,
                body: JSON.stringify({
                    message: (<Error>error).message
                })
            }
        }
        
    }else if(eventResource === '/products/{id}'){
        const productId = event.pathParameters?.id
        if (event.httpMethod === 'PUT') {
            console.log(`PUT product ${productId}`)
            try {
                const product = JSON.parse(event.body!)
                const productUpdated = await productRepository.update(productId!, product)

                return {
                    statusCode: 200,
                    body: JSON.stringify(productUpdated)
                }
            }catch(error) {
                return {
                    statusCode: 400,
                    body: JSON.stringify({
                        message: (<Error>error).message
                    })
                }
            }
        }

        if (event.httpMethod === 'DELETE') {
            console.log(`DELETE product ${productId}`)
            try {
                 await productRepository.delete(productId!)
                return {
                    statusCode: 204,
                    body: JSON.stringify({
                        message: 'Product deleted'
                    })
                }
            }catch(error) {
                return {
                    statusCode: 400,
                    body: JSON.stringify({
                        message: (<Error>error).message
                    })
                }
            }
        }

    }
    return {
        statusCode: 400,
        body: JSON.stringify({
            message: 'Bad Request'
        })
    }
}