import {APIGatewayProxyEvent, APIGatewayProxyResult, Context} from 'aws-lambda';
import {ProductRepository, ProductRepositoryImpl} from "opt/nodejs/productsLayer"
import { DynamoDB } from "aws-sdk";

const productsDatabaseName = process.env.PRODUCTS_TABLE_NAME!
const productsRepository = new ProductRepositoryImpl(new DynamoDB.DocumentClient(), productsDatabaseName)

export async function handler
    (event: APIGatewayProxyEvent, context: Context): Promise<APIGatewayProxyResult> {

        // lambdaRequestId é o ID da requisição do Lambda. É um ID único gerado pelo AWS Lambda para cada requisição
        const lambdaRequestId = context.awsRequestId
        // eventRequestID é o ID da requisição do Evento. É um ID único gerado pelo AWS API Gateway para cada requisição
        const eventRequestID = event.requestContext.requestId

        console.log(`Lambda Request ID: ${lambdaRequestId}\nEvent Request ID: ${eventRequestID}`)

        if(event.resource === '/products'){
            if (event.httpMethod === 'GET'){
                console.log('GET products')
                const products = await productsRepository.findAll()
                return {
                    statusCode: 200,
                    body: JSON.stringify(products)
                }
            }
        }else
            if (event.resource === '/products/{id}'){
                const productId = event.pathParameters?.id!
                
                try{
                    const productFound = await productsRepository.findById(productId)
                    return {
                        statusCode: 200,
                        body: JSON.stringify(productFound)
                    }
                }catch(error){
                    return {
                        statusCode: 404,
                        body: JSON.stringify({
                            message: (<Error>error).message
                        })
                    }
                }
            }

        return {
            statusCode: 501,
            body: JSON.stringify({
                message: 'Not implemented'
            })
        }
    }
