#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { ApiGatewayStack } from '../lib/api-gateway-stack';
import { ProductsAppStack } from '../lib/products-app-stack';
import  { ProductAppLayerStack } from "../lib/product-app-layer";

const app = new cdk.App();

const env: cdk.Environment = {
    account: '486320211311',
    region: 'sa-east-1'
}

const tags = {
    cost: "Ecommerce",
    team: "my-team"
}

const productsAppStack = new ProductsAppStack(app, 'ProductsAppStack', {env, tags })

const apiGatewayStack = new ApiGatewayStack(app, 'ApiGatewayStack', {
    env,
    tags,
    productsFetchHandler: productsAppStack.productsFetchHandler,
    productsAdminHandler: productsAppStack.productsAdminHandler
})

const productAppLayerStack = new ProductAppLayerStack(app, 'ProductAppLayer', { env, tags })

productsAppStack.addDependency(productAppLayerStack, 'ProductAppLayerStack')

apiGatewayStack.addDependency(productsAppStack, 'ProductsAppStack')

/* If you don't specify 'env', this stack will be environment-agnostic.
   * Account/Region-dependent features and context lookups will not work,
   * but a single synthesized template can be deployed anywhere. */

/* Uncomment the next line to specialize this stack for the AWS Account
 * and Region that are implied by the current CLI configuration. */
// env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION },

/* Uncomment the next line if you know exactly what Account and Region you
 * want to deploy the stack to. */
// env: { account: '123456789012', region: 'us-east-1' },

/* For more information, see https://docs.aws.amazon.com/cdk/latest/guide/environments.html */